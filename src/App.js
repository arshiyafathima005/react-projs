// useContext Api-
import React,{createContext, useState} from 'react'
import UsecontextApi from './component/UsecontextApi';
import OtherChild from './component/OtherChild';
import UseeffectVsUsememo from './component/UseeffectVsUsememo';
import { Home } from './component/customHook/Home';
import { HomeWithCstmHook } from './component/customHook/HomeWithCstmHook';
import UseRefs from './component/useRefHook/UseRefs';
import { UseRefChangeStyle } from './component/useRefHook/UseRefChangeStyle';
import UseCallbackHook from './component/useCallBack/UseCallbackHook';
import UseMemo from './component/useMemoHook/UseMemo';
import { CompA } from './component/contextApi/CompA';
import { ControlledComp } from './component/controlled-uncontrolled/ControlledComp';
import { UncontrolledComp } from './component/controlled-uncontrolled/UncontrolledComp';
import Hoc from './component/HOC/Hoc';
import Counter from './component/HOC/Counter';
import HocGreen from './component/HOC/HocGreen';
import Componetsnt from './component/components-QA/Componetsnt';
import Parent from './component/childToParent/Parent';

export const GlobalInfo=createContext();
const FirstName=createContext();
const LastName=createContext();


const App = () => {
  // const [color,setColor]=useState('green');
  // const [day,setDay]=useState("Monday");
  // const getDay=(item)=>{
  //   console.log(item);
  //   setDay(item)
  // }
  return (
    <>
    <Parent/>
    {/* <Hoc counter={Counter}/>
    <HocGreen counter={Counter}/>
    <Componetsnt/> */}
   {/* <ControlledComp/>
    <UncontrolledComp/> */}
     {/* // EXpamle 2 context Api */}
    {/* <FirstName.Provider value={"Arshiya"}>
      <LastName.Provider value={"Fathima"}>
      <CompA/>
    
      </LastName.Provider>
    </FirstName.Provider> */}
   
  
   {/* <Home/> */}
    {/* <UseRefs/> */}
      {/* <UseRefChangeStyle/> */}
      {/* <UseCallbackHook/> */}
      {/* <UseMemo/> */}
    {/* <HomeWithCstmHook/> */}
    {/* <UseeffectVsUsememo/> */}
    </>
    // <GlobalInfo.Provider value={{AppColor:color,getDay:getDay}}>
    //    <div>
    //     <h1>Parent Component {day}</h1>
    //     <UsecontextApi/>
    //     <OtherChild/>
    //   </div>
    // </GlobalInfo.Provider>
   

  );
}

export default App;
export {FirstName,LastName}




// useEffect- we can use it for life cycle method functionality in functional component
// import React,{useEffect, useState} from "react";
// function App()
// {
//   const [count,setCount]=useState(0)
//   // React.useEffect(()=>{
//   useEffect(()=>{
//     console.log("useEffect in react ")
//   },[])
//   useEffect(()=>{
//     console.log("useEffect in react1 ")
//   })
//   return(
//     <div>
//       <h1>useEffect {count}</h1>
//       <button onClick={()=>setCount(count+1)}>Update count</button>
//     </div>
//   )
// }
// export default App;
// useState-to track the state of function components
// import React,{useState} from 'react';
// function App(){
//   const [data,setData]=useState('Arshiya')
//   return(
//     <div>
//       <h1>Hello {data}!</h1>
//       <button onClick={()=>setData("Fathima")}>Update Data</button>
//     </div>
//   )
// }

// import React from 'react';
// import UseStateHook from './component/useStateHook';
// import UseEffect1 from './component/UseEffect/useEffect1';

// const App = () => {
//   return (
//     <>
    
//       {/* <UseStateHook/> */}
//       <UseEffect1/>
//     </>
//   )
// }

// export default App;

// import React, { useState } from 'react';

// // NOTE:HOOKS can be called only inside the body of functional component
// // const state=useState();
// // console.log(state);

// // let count=1;
// // const IncNum =()=>{
// //   console.log('clicked'+count++);
// // }

// const App = () => {
//   // const state=useState();
//   // const -current state,setCount-updated state,5- intial state
//   const [count,setCount]=useState(0);

//   const IncNum =()=>{
//       setCount(count+1);
//       // console.log('clicked'+count++);
//     }

//   const name=['vi','no','d'];

//   return (
//     <div>
//       <h1> {count} </h1>
//       <button onClick={IncNum}> Click </button>
//     </div>
//   )
// }

// export default App;


// import React from 'react';
// import Testtodo from "./component/todo";

// function App() {
//   return (
//     <>
//       <Testtodo/>
//     </>
//   );
// }

// export default App;
