import React from 'react';
import HookRule from './HookRule';

const useStateHook = () => {
  return (
    <div>
      <HookRule/>
    </div>
  )
}

export default useStateHook;


// import React, { useState } from 'react'

// const useStateHook = () => {
//     const [toggleState,setToggleState]=useState('Arshiya');
//     const changeState=()=>{

//         toggleState==='Arshiya'?setToggleState('Fathima'):setToggleState('Arshiya');
//     }
//   return (
//     <>
//       <div>{toggleState}</div>
//       <button onClick={changeState}>Click Me</button>
//     </>
//   )
// }

// export default useStateHook;
