1.What are lifecycle methods in react?
Predefined methods of components that can monitor and update components.
ex:fetch data ,update data
import React from 'react';

useEffect()=>{
    console.log("life cycle method is called")
}
const Ex1=()=>{
return(
<></>
)
}

export default Ex1;

2.Can we use lifecycle methods with functional components?
Yes we can use,

3.what are the phases of lifecycle methods?
1.Mounting-when ever component is created
2.updating-when ever state or prop get updated it updates our component
3.Unmounting-Whenever our component is removed from the dom unmount method is called

4.When a life cycle method will run and what we can do inside a life cycle method?
When a component create ,updated or remove at that time lifecycle method will call
we can update state,update variables,call API etc

5.Can we call a life cycle method inside another life cycle method?
No we cannot call any other life cycle method inside another one because they are independent each other

6.Call useEffect when components created only?
useEffect(()=>{
    console.log('created only')
},[])

7.Call useEffect when any state or props will update?
useEffect(()=>{
    console.log('created only')
})

8.Call useEffect when any specific state will update?
const [name,setName]=useState("");
const [email,setEmail]=useState("");

useEffect(()=>{
    console.log('created only')
},[name])

return(
    <button onClick={()=>setName('Arshiya')}>Name</button>
    <button onClick={()=>setEmail('arshiya@gmail.com')}>Email</button>
)

9.Call useEffect when component is unmount?
useEffect(()=>{
    return ()=>{
        //your code component unmounted(compoent removed from dom)
    }
})

10.How can useEffect make an infiniteloop and how to solve this problem?
const [name,setName]=useState("");
useEffect(()=>{
    console.log('created only')
    setName(Math.random())//component will updated and redender again and again
})
return(
    <button onClick={()=>setName('Arshiya')}>Name</button>)

SOLVE,
1.CALL ONLY when component is created 
useEffect(()=>{
    console.log('created only')
    setName(Math.random())//component will updated and redender again and again
},[])

2.put any other state
useEffect(()=>{
    console.log('created only')
    setName(Math.random())//component will updated and redender again and again
},[x])

11.Can we make more than one useEffect in a component?
Yes, As many as we can use ,with different contionals also we can add
useEffect(()=>{
    console.log('created only')
   },[])

   useEffect(()=>{
    console.log('created only')
   },[x])

   useEffect(()=>{
    console.log('created only')
   })

12.Can we call a useEffect on button click?
no, for that you need to pass a function inside the array of useeffect hook like below:

function handleClick(){
    console.log("clicked");
}

useEffect(() => {
    handleClick();
}, [handleClick]);

or call with any state

useEffect(() => {

}, [state]);

<button onclick={handleClick()}>Click me</button>

13.When useEffect hooks are introduced?
in REact 16.3 version

14.Can we use hooks in class components?
No

15.How do you identify its a hook or not?
by looking its name ,hooks usually starts with "use"
