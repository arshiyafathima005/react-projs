import React, { useEffect, useState } from 'react'

const UseEffect1 = () => {

    const[count,setCount]=useState(0);
    const[num,setNum]=useState(0);
    //1.When we load the page for first time useEffect is called
    // 2.Side effect ,first other things like console.log outside useffect will be executed then use effect will execute
    // 3.When ever the render fucntion is called useEffect works
    // 4.User effect alwys expect a function
    // 5.[] - works only when you refresh the page for first time [page render for the first time]
    useEffect(()=>{
      alert('You clicked me');
      document.title=`you clicked me ${count}`
    },[count]);
    // console.log('Hello outside');
  return (
    <>
      <h1>{count}</h1>
      <button onClick={()=>{setCount(count+1)}}>Click {count}</button>
      <br/>
      <button onClick={()=>{setNum(num+1)}}>Click {num}</button>
    </>
  )
}

export default UseEffect1;
