import React, { useState, useRef } from 'react';

const Counter = () => {
  const [count, setCount] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const intervalRef = useRef(null);

  const startCounter = () => {
    if (!isActive) {
      intervalRef.current = setInterval(() => {
        setCount(prevCount => prevCount + 1);
      }, 1000); // Change the interval duration as needed (in milliseconds)
      setIsActive(true);
    }
  };

  const stopCounter = () => {
    clearInterval(intervalRef.current);
    setIsActive(false);
  };

  const resetCounter = () => {
    clearInterval(intervalRef.current);
    setIsActive(false);
    setCount(0);
  };

  return (
    <div>
      <h1>Counter: {count}</h1>
      <div>
        <button onClick={startCounter} disabled={isActive}>
          Start
        </button>
        <button onClick={stopCounter} disabled={!isActive}>
          Stop
        </button>
        <button onClick={resetCounter}>
          Reset
        </button>
      </div>
    </div>
  );
};

export default Counter;
