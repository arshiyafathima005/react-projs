import React from 'react'
import { FirstName ,LastName} from '../../App' 

export const CompC = () => {
  return (
    <>
    {/* callback hell one inside another lenthy */}
     <FirstName.Consumer>{(fname)=>{
        return  (
          <LastName.Consumer>
            {(lname)=>{
              <div>My name is {fname} {lname}</div>
            }}
          </LastName.Consumer>
        )
      }}
      </FirstName.Consumer>
      
    </>
   
   
  )
}
