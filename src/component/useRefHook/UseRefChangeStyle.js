import React ,{useRef,useState} from 'react'

export const UseRefChangeStyle = () => {
    const [myData,setMydata]=useState("")
    const inputElem=useRef("");
    const changeColor=()=>{
        console.log(inputElem.current);
        inputElem.current.style.backgroundColor="yellow";
        inputElem.current.focus();
    }
  return (
    <>
    <input
    ref={inputElem}
    type='text'
    value={myData}
    onChange={(e)=>setMydata(e.target.value)}/>
     <a href='https://www.w3schools.com/react/react_useref.asp'>af</a>
    <button onClick={changeColor}>Submit</button>
    </>
    
  )
}
