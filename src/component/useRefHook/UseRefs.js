import React ,{useEffect, useState,useRef}from 'react'

const UseRefs = () => {
    const [myData,setMydata] =useState("");
    // const [count,setCount]=useState();
    const count =useRef(0);
    console.log("use ref is an object it will return current value",count);
    // use ref is an object it will return current value
     // const [count,setCount]=useState(0);
    //  useEffect(()=>{
    //     setCount(count+1)
    // },[]) dependency array is added so it is prevented otherwise it will go again
    // better use useRef
    // if i add 0 it will go to infinite loop
     // when ever the component is rendered it will call and go to infinite loop


    useEffect(()=>{
        count.current=count.current+1;
        // setCount(count+1)
    })

  return (
    <>
      <input
      type='text'
      value={myData}
      onChange={(e)=>setMydata(e.target.value)}/>
      {/* <p>{count}</p> */}
      <p>{count.current}</p>
     
    </>
    
  )
}

export default UseRefs;
