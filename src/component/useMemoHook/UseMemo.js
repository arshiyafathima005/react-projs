import React, { useState ,useMemo} from 'react'

const UseMemo = () => {
    const [myNum,setmyNum]=useState(0);
    const [show,setShow]=useState(false);

    const getValue=()=>{
        return setmyNum(myNum+1);
    }

    const countNumber=(num)=>{
        // just to delay the rendering using for loop
        console.log("Memo.js countNmber -num ",num);
        for(let i=0;i<=10000000;i++){}
        return num;
    }

    const checkData=useMemo(()=>{
       countNumber(myNum);
    },[myNum])
    // const checkData=countNumber(myNum)

  return (
    <>
      <button onClick={getValue} style={{backgroundColor:"red"}}>Counter</button>
      <p>My new number : {checkData}</p>
      <button onClick={()=>setShow(!show)} >
        {show?"You clicked me":"Click me plz"}
      </button>
    </>
  )
}

export default UseMemo
