import React, { useEffect, useState } from 'react';
// import todo from "../images";
import "../App.css";

// to get data from local storage
// const getLocalItems=()=>{
//     let list=localStorage.getItem('lists');
//     console.log(list);
//     if(list){
//         return JSON.parse(localStorage.getItem('lists'));
//     }else{
//         return [];
//     }
// }

const Todo=()=>{
    const [inputData,setInputData]=useState('');
    const [items,setItem]=useState([]);
    const [toggleSubmit,setToggleSubmit]=useState(true);
    const [isEditItem,setIsEditItem]=useState(null);

    const addItem=()=>{
        // setItem(inputData) takes in form of string so
        if(!inputData){
          alert('plz fill data');
        }else if(inputData && !toggleSubmit){
            setItem(items.map((elem)=>{
                if(elem.id===isEditItem){
                    return {...elem,name:inputData}
                }
                return elem;
            }))
            setToggleSubmit(true);
            setInputData('');
            setIsEditItem(null);
        }
        else{
            const allInputData={id: new Date().getTime().toString(),name:inputData};
            // setItem([...items,inputData])//previous data was removed when we add new data so use ...(spread oprtator)
            setItem([...items,allInputData])//previous data was removed when we add new data so use ...(spread oprtator)
            setInputData('');
        }
         }
    //  const deleteItem=(id)=>{
    //     console.log('')
    //     const updatedItems=items.filter((ele,indx,arr)=>{
    //         return indx!=id;
    //     })
    //     setItem(updatedItems);
    //  } 

      const deleteItem=(index)=>{
       const updatedItems=items.filter((elem)=>{
            return index!==elem.id;
        })
        setItem(updatedItems);
     } 

    //  edit item
    const editItem=(id)=>{
        let newEditItem=items.find((elem)=>{
            return elem.id===id;
        })
        console.log(newEditItem);
        setToggleSubmit(false);
        setInputData(newEditItem.name);
        setIsEditItem(id);
    }
     
     //remove all
     const removeAll=()=>{
        setItem([]);
     }

    //  add data to localStorage
    useEffect(()=>{
        localStorage.setItem('lists',JSON.stringify(items))
    },[items])
    return(
        <>
          <div className='main-div'>
            <div className='child-div'>
          
                <figure>
                <span className="glyphicon glyphicon-envelope"></span>
                <figcaption>Add Your List Here </figcaption>
                    {/* <img src='' alt=''/> */}
                </figure>
                <div className='addItems'>
                    <input type="text" placeholder=" Add Items... " id="" value={inputData} onChange={(e)=>setInputData(e.target.value)}/>
                   {toggleSubmit? <i className='fa fa-plus add-btn' title='Add Item' onClick={addItem}></i>:<i className='far fa-edit add-btn' title='Edit Item' onClick={addItem}></i>
                                  }
                  
                </div>
                <div className="showItems">
                    {
                        items.map((elem)=>{return(
                          <div className='eachItem' key={elem.id}>
                                <h3>{elem.name}</h3>
                                <div className='todo-btn'>
                                    <i className='far fa-edit add-btn' title='Edit Item' onClick={()=>editItem(elem.id)}></i>
                                    <i className='far fa-trash-alt add-btn' title='Delete Item' onClick={()=>deleteItem(elem.id)}></i>
                                </div>
                            </div>
                        )})
                        // items.map((elem,indx,arr)=>{return(
                        //   <div className='eachItem' key={indx}>
                        //     <h3>{elem}</h3>
                        //     <i className='far fa-trash-alt add-btn' title='Delete Item' onClick={()=>deleteItem(indx)}></i>
                        // </div>
                        // )})
                    }
                 
                </div>
                {/* clear button */}
                <div className='showItems'>
                    <button className='btn effect04' data-sm-link-text="Remove All" onClick={removeAll}><span>CHECK LIST</span></button>
                </div>
            </div>
           </div>
        </>
        
    )
}
export default Todo;