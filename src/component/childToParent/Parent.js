import React ,{useState}from 'react'
import ChildToParent from './ChildToParent'

const Parent = () => {
    const [message,setMessage]=useState("initial text");
    const callBackFunction=childData=>{
        setMessage(childData);
    }
  return (
    <div>
      <p>Parent Message:{message}</p>
      <ChildToParent parentCallback={callBackFunction}/>
    </div>
  )
}

export default Parent
