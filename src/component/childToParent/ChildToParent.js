import React,{useState} from 'react'

const ChildToParent = ({parentCallback}) => {
  const [newMsg,setNewMsg]=useState("");
  const sendData=()=>{
    parentCallback(newMsg)
  }
  return (
    <div>
      <input onChange={event=>setNewMsg(event.target.value)}/>
      <button onClick={()=>sendData()}>send to parent</button>
    </div>
  )
}

export default ChildToParent;


//EX1:Good and easy
// child 
// import React from 'react'

// const ChildToParent = ({getChildValue}) => {

//   return (
//     <div>
//       <input onChange={getChildValue}/>
//      </div>
//   )
// }

// export default ChildToParent;



// /parent
// import React, { useState } from "react";
// import Child from "./Child";
// const Parent = () => {
//   const [data, setData] = useState("Hi");
//   const callBackFun = (e) => {
//     setData(e.target.value);
//   };
//   return (
//     <div>
//       <h1>{data}</h1>
//       <Child getChildValue={callBackFun} />
//     </div>
//   );
// };
// export default Parent;


// APP
// import "./styles.css";
// import Parent from './Parent'

// export default function App() {
//   return (
//     <div className="App">
//       <Parent/>
//     </div>
//   );
// }
