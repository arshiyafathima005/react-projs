import React,{useEffect,useMemo,useState} from 'react'


// NOTE:useEffect prevents the entire component from unwanted rerendering
// useMemo prevents only the specific part of the component
const UseeffectVsUsememo = () => {

    const [count,setCount]=useState(0);
    const [name,setName]=useState('');
    const noRender=useMemo(()=>{
        return <div>
             <h1>useEffect vs useMemo</h1>
            <h1 style={{color:'red'}}>Dont rerender again{count} {name}</h1>
        </div>
    },[name]);
    //onlty when name is updated then it will update the specific compnent

    // const [data,setData]=useState(100);
    // useEffect(()=>{
    //     console.log(Math.random());
    // },[data]);
    // only when data button is clicked then entire component rerenders
    // on count click only it will rerender the component at the first time when appliaction is loaded
  return (
    <div>
        {/* if i want to prevent only some specific part from rerendering then i will go with useMemo */}
    {noRender}
     <h1>useEffect vs useMemo</h1>
    <h1>Render it again and again {count}</h1>
      {/* <h5>{count}</h5> */}
      {/* <h5>{data}</h5> */}
      <button onClick={()=>{setCount(count+1)}}>count</button>
      <button onClick={()=>{setName('arshiya')}}>Update name</button>
      {/* <button onClick={()=>{setData(count+1)}}>Update</button> */}
    </div>
  )
}

export default UseeffectVsUsememo
