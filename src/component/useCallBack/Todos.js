import React, {memo} from 'react'

const Todos = ({todos,addTodo}) => {
    console.log("child render");
  return (
    <>
      <h2>My todo</h2>
      {todos.map((todo,index)=>{
        return <p key={index}>{todo+index}</p>
      })}
      <button onClick={addTodo}>Add</button>
    </>
  )
}
//memo is higher order function which prevents the re-rendering of the component
export default memo(Todos)


// import React, { useState, useCallback, useMemo } from 'react';

// const ExampleComponent = () => {
//   const [count, setCount] = useState(0);
//   const [input, setInput] = useState('');

//   // useCallback example
//   const handleClick = useCallback(() => {
//     setCount(count + 1);
//   }, [count]); // Dependency array - recreates handleClick only when count changes

//   // useMemo example
//   const doubleCount = useMemo(() => {
//     console.log('Computing doubleCount');
//     return count * 2;
//   }, [count]); // Recomputes doubleCount only when count changes

//   return (
//     <div>
//       <p>Count: {count}</p>
//       <p>Double Count: {doubleCount}</p>
//       <button onClick={handleClick}>Increment Count</button>

//       <input
//         value={input}
//         onChange={(e) => setInput(e.target.value)}
//         placeholder="Type something..."
//       />
//     </div>
//   );
// };

// export default ExampleComponent;
