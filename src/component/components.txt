1.What are components?
A piece of code that can be reused like functions
but more powerfull than functions
2 types 
class and functional components

1.Can we create a multiple components in a single file?
yes,but no need to export it

2.Can we create a component inside component?
Yes,but no need to export it

3.Can we conditionally render the component?
Yes,

4.Can we use Let,var and const instead state and props?
Yes we can,
*when state is changed entire component will rerender ,but if we variables are changed components will not rerender

5.What is HOC?
HOC-Higher order components in react is a advanced technique for reusing component logic
A component will take the component as a prop(parmeter) and return the other component

6.What is the Controlled Component?
A component that have a input form and these input fields are controlled by react state

7.What are uncontrolled component?
A component that has an input form and these input fields are controlled by ref OR DOM.

8.What are pure components?
Pure components in react are the components which do not re-renders when the values of state and props has been updated with the same value.

9.Can we pass HTML contents as props in components?
yes

10.What are dumb components?
A dumb components can be defined very easily as a stateless functional components
A functional component which dose'nt have any state(means any state is not used in it)

11.How we can do component communication and types of component communication?
Passing data between 2 componenets or more than 2 components
We can use props and types of communication are
1.Parent - child
2.Child - Parent
3.Sending data between siblings

12.What is the role of props in component communication?
*Only with props we can send data between componenets
*Props are read only

13.Can you modify the props ?
No you can not modify the props ,they are read-only
Comp(A)->sibling(c)

14.What is lifting up state?
*Sending data from child to parent component is lifiting up state
*We can do it with the help of props,state can not access outside the components
Sibling(c)->parent(A)

15.Can you do lifiting up state with the help of state?
No ,we can use only props ,because state can't access outside the components ,it can be accessable only inside components

16.How we can send data from child to parent component?
we need to create a function and pass it as an parameter (from child to parent)
App(
    const sum=(data)=>{
        console.log(data)
    }
    <Child sum={sum}>
    <Child sum={sum}>
    <Child sum={sum}>
)

Child(
    const data=200;
    return(
        <button onClick={()=>props.sum(data)}>Call sum</button>
    )
)


17.How we can send function from parent to child component?
App(
    const sum=(a,b)=>{
        return (a+b)
    }
    <Child sum={sum}>
    <Child sum={sum}>
    <Child sum={sum}>
)

Child(
    return(
        <button onClick={()=>props.sum(2,3)}>Call sum</button>
    )
)

17.why do we use function inside parent?
because if we are using same functionality in multiple compoents the we keep that function in the parent component and pass it as prop ,then use it

18.How we can send component from parent to child component?
App=()=>{
    return(
    <Child dummy={Dummy}>

)
}
const Dummy=()=>{
    return(
        <h1>Dummy component</h1>
    )
}

Child=(props)=>{
    return(
        <props.dummy/>
        <button onClick={()=>props.sum(2,3)}>Call sum</button>
    )
}

19.How we can reuse the components in a loop?
we can use map method to loop compoents
App=()=>{
    return(
        {
            ['a','b','c'].map((item)=> <Child item={item}/>)

        }
)
}

Child=(props)=>{
    return(
      <h1>{props.item}</h1>
    )
}

13.Show component conditionally?
App=()=>{
    return(
        {
            ['a','b','c'].map((item)=> item ==='a' && <Child item={item}/>)

        }
)
}

Child=(props)=>{
    return(
      <h1>{props.item}</h1>
    )
}