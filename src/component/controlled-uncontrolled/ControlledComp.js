import React,{useState} from 'react'

export const ControlledComp = () => {
    const [name,setName]= useState("");
    const [fullName,setFullName]=useState();
    const inputEvent=(event)=>{
        setName(event.target.value)
        // console.log(event.target.value);
    }

    const onSubmit=()=>{
      setFullName(name);
    }
  return (
    <>
      <form>
        <h1>Controlled component</h1>
        <h2>Hello {fullName}</h2>
        <input type="text" placeholder='Enter your name' value={name} onChange={inputEvent}/>
        <button onClick={onSubmit}>Click Me</button>
      </form>
    </>
  )
}
