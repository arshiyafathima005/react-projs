import React,{useRef} from 'react'

export const UncontrolledComp = () => {
    const inputs=useRef(null);
    const submit=()=>{
     alert(inputs.current.value);  
    }

  return (
    <>
        <form onSubmit={submit}>
            <h1>Uncontrolled component</h1>
            <input type="text" ref={inputs}/>
          <input type='submit' value="Submit"/>
        </form>
    </>
  )
}
