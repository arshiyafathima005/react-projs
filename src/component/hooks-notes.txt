1.What are hooks?
Hooks are reuseable functions .basically to manage the components state
hooks will start wit the keyword use

2.Why use Hooks?
To re-use logic between different components, without having to create a new component and also helps in managing side effects in functional components.

3.What is useState hook?
This is a hook for use state in functional components
ex:const [count,setCount]=React.useState(0);

1.What is useEffect hook?
we can perform sideeffects in fucntion components
You can tell React that your component needs to do something after render
It we can pass the dependecy to execute it only once
you can think of useEffect Hook as componentDidMount, componentDidUpdate, and componentWillUnmount combined.
to use life cycle methods in functional components

1.2.Write a useEffect code and console some value?
1.3.LFC in functional components
useEffect(()=>{
      //mounting phase
},[])

useEffect(()=>{
      //updating state
},[state])

useEffect(()=>{
      return ()=>{
      //unmounting phase
      }
},[])


1.3.why the console is rendered twice in react js?
Because of Strict mode - React will render twice. So if you update a state, 
it will render twice if what you're rendering is affected by that state. 
If you go to Index. jsx and remove StrictMode around the rendering of App, you will see it will only console.

1.5.How does useEffect get triggered?
The hook useEffect is called after each time your component renders. 
It is possible to specify which data modification triggers the effects run in useEffect with a list of dependencies. 
An empty list of dependencies lets you run an effect only the first time your component is rendered.

1.6.From where the applications flow will start in react js?


1.7.How do I stop multiple rendering in react JS?
Use `useMemo` and `useCallback` hooks: The `useMemo` hook allows you to memoize values and only recompute them if their dependencies change. ...
Optimize component state: Ensure that you are managing component state efficiently.

1.8.What is the use of useDispatch and useSelector in redux?

2.What is useRef hook?
It create a mutable varaible which will not re-render the components
useRef return only object or  1 item, i.e called current (ex const count=useRef(0),count.current)
It can be used to access the DOM elements directly by using ref as an attribute inside the element



3.Why we want useRef explain with ex?Where to use ref
1.It will not re-render the components
 It can be used to store a mutable value that does not cause a re-render when updated.
ex:If counting how many time our application renders using useState hook,then it will leads to infinite loop
since use state will cause always rerender

4.What is useCallback hook?
It is mainly used to improve the application performance 
It returns a memorized callback function
cache function

Real Example:

5.Difference b/w useCallback and useMemo hook?
The useCallback and useMemo Hooks are similar. The main difference is that useMemo returns a memoized value and useCallback returns a memoized function


6.Why we use callback and memo hooks?
To increase the performance of our application

7.Give real project examples of useCallback and useMemo hook usage?
Image galery functions


x:callback : if we are having two components in a single file and we are updating the state of 1 component then another component function is which is in parent component is hoiseted(hoiseted :all the parent functions local and global variables are hoisted(available) to child function) and also rerenderd because of useState to prevent this we use this hooks
Ex:Callback: If we setting up music in our home its disturbing nabours then its not good to fix the  


7.What is useMemo?
It returns the memorized value
To increase the performance of application
useMemo is same as useEffect (uses callback and dependency)

RealExample:

8.Difference b/w useMemo and useEffect?
* In useEffect we can't return the data ,but in useMemo we can return value
* In useEffect we can't store the data in a variable ,but in useMemo we can store the value in variable
Re-render will also not sideeffect in useMemo
*useEffect prevents the entire component from unwanted rerendering ,useMemo prevents only the specific part of the component

9.What is memorization in react js?
memorization is an optimization feature in react which,when used in the right place increase the performance of program

10.What is React memo?
memo is higher order function which prevents the re-rendering of the component
Why memo?memo will cause React to skip rendering a component if its props have not changed.

11.What is Context api in react?
Context API allows you to easily access data at different levels of component tree,without passing prop to every level
ex:Parend Component-> Child Comp a->Child comp b->child comp c (Pass data from Pc - Child comp c)[game:playing from ear to ear data pass]
Steps: 1.createContext()-to pass data
2.provider -to pass data we need a provider
3.consumer-to access the data(note :when ever a consumer is used it accepts a function)

12.What is prop drilling?How to solve it ?
Prop drilling is when a parent component passes data down to its children and then those children pass the same data down to their own children
This process can continue indefinitely
Long chain of component dependencies ->difficult to manage and maintain

ex:Parent data---> child (data) --->own child

Solve it using useContext hook. 
The useContext hook is based on Context API and works on the mechanism of Provider and Consumer.


13.What is useContext hook?
It helps us to consume context data easily at different levels of components tree,without passing prop to every level

14.Diffe b/w context hook and conext api
It is similar to context api but better that context api in consuming the context data 
In useContext we should use useContext to consume the data , but in use Context api we need to use Consumer which will make the process of consumer little harder to consume the data

15.What is useReducer Hook?
Reducers are pure functions that takes a state and action and return a new state
It is same as useState hook ,but to mange the multiple states in application then we go for useReducer

16.Dif b/w useContext 

Redux

useContext is a hook.	Redux is a state management library.
It is used to share data.	It is used to manage data and state.
Changes are made with the Context value.	Changes are made with pure functions i.e. reducers.
We can change the state in it.	The state is read-only. We cannot change them directly.
It re-renders all components whenever there is any update in the provider’s value prop.	It only re-render the updated components.
It is better to use with small applications.	It is perfect for larger applications. 
It is easy to understand and requires less code.	It is quite complex to understand.

15.How to use useReducer hook?
1.Initial state
2.useReducer hook which takes 2 args (reducer function and initial state)
3.reducer function has 2 parameters (state and action)
4.reducer function always return some thing
5.useReducer returns (state and dispatch)
6.state - is the data of component which is changed
7.dispatch -  dispatch will trigger the action method by using type 


16.What is pure function?
The function does not produce any side effects or not modify the state of the application
The function always returns same output if the same arguments are passed in

17.Differences between Pure and Impure Functions:
  Features                            Pure Functions                                     Impure Functions
State Modification	Does not modify the state of the application	It can modify the state of the application
Dependency	        Only depends on its input parameters	        It  can be dependent on other parts of the code
Testing	            Easier to test	                                harder to test
Maintenance	        It is easier to maintain	                    It is harder to maintain

18.Custom hooks?
When you have a login which is used in multiple components ,then we can extract that logic to a custom hook

19.How to create custom hooks?
Custom hooks starts with "use" ex:useFetch

building a custom hook:refere the folder custohook

20.What are Controlled vs Uncontrolled Components in ReactJS?

Difference between Controlled and Uncontrolled Components:
         Controlled  Component                                                                           Uncontrolled Component
The component is under control of the component’s state.	                                Components are under the control of DOM.
These components are predictable as are controlled by the state of the component.	        Are Uncontrolled because during the life cycle methods the data may loss
Internal state is not maintained	                                                        Internal state is maintained
It accepts the current value as props	                                                    We access the values using refs
Does not maintain its internal state.	                                                    Maintains its internal state.
Controlled by the parent component.	                                                        Controlled by the DOM itself.
Have better control on the form data and values	                                            Has very limited control over form values and data


21.What is controlled component?
In controlled component form data is handled by react component
EX:Input form element is controlled by react then its called as controlled component.
Single source of truth-Controlled components have a one-way data flow where the state inside the component serves as the single source of truth.
The parent component has held over the form data in the controlled component
each time when value of inp field is changed then it should not submit to server ,only on final submition it should submit the calue



22.What is uncontrolled component?
Form data is handled by DOM it self.
The DOM itself handles all the form data in an uncontrolled component.
NOTE:If we are using states in 2 components and if we are using refs in 2 components then it is called as uncontrolled component


23.Why is the store known as a single source of truth in a Redux application?
In a Redux application, there is a single source of truth for the entire application. 
This source of truth is called the store and is an object that holds the entire state of the application. 
All components in the application can access this state through the store, and changes to the state can be made through actions.

24.What is a single source of truth for states in Redux?
1 Single source of truth. The state of the entire application is stored in an object tree within a single store.

25.What are Redux Three Principles?
1.Single source of truth-The state of the entire application is stored in an object tree within a single store.
2. State is read-only-The only way to change the state is to emit an action that describes what happened.
3.Changes are made with pure functions-You write reducers as pure functions to specify the concrete way the state tree is transformed by action.
Reducers are pure functions that take a previous state and action and return a new state. Keep in mind that you must return a new state object instead of changing the old state.
“Given the same arguments, it should calculate the next state and return it. No surprises. No side effects. No API calls. No mutations. Just a calculation.”


26.What are the advantages of a single state tree?
 This makes it easier to debug applications or perform internal inspections and to easily implement some features that were previously difficult to implement (for example, undo / redo).

27.What are statefull(container components) and stateless components (dumbed compoennts)?
STATEFULL-Any component is managed by state is called as statefull component 
Statefull comps are Container components which holds other components or dummies
STATELESS-A components with don't have any state it just render the UI that is called stateless comp or dummed components

28.What is HOC in reactjs?
HOC-Higher order components in react is a advanced technique for reusing component logic
A component will take the component as a prop and return the other component

29.What is server side rendering in react js?
SSR is used to render the web page on the server before sending them to client.
This allows for faster page load,
Improve performance
Allows SEO- friendly rendering solution for react applications

30.What are the features of react?
JSX,
SSR,
Single page application
One way data binding,
Performance ,
Extensions,
Components,
Conditional statements,
Simplicity,
Learn faster,
Easy to understand

31.What is virtual dom and dom?
JavaScript Frameworks updates the whole DOM at once, which makes the web application slow.
 But react uses virtual DOM which is an exact copy of real DOM. 
 Whenever there is a modification in the web application,
  the whole virtual DOM is updated first and finds the difference between real DOM and Virtual DOM.
   Once it finds the difference, then DOM updates only the part that has changed recently and everything remains the same. 

38.How the virtual DOM is faster than real dom as it is a two step procees 1st update VDOM then DOM?
  less rendering makes faster to load the page helps to improve the performace of application and more responsive, 
  How?  only the updated components are rendered in the virtual DOM and then updated in real DOM , but directly updating entier DOM slow down the process.

39.Can more than 1 vdom IS created in react?
  Yes,by using relavent algos,(as a developer no need to bother about it its the job of react)


32.What is JSX?
Javascript syntax extension.its the combination of html and js

33.Does browser supports JSX ?
No,Browser reads on JS object
as a result babel compailer transcomplie the code into JS code



34.What is the main d/f b/w React.memo and Pure components?
React.PureComponents only check shallow comparision and react.memo will to deep comparision also
PureCompeonent are used in class comoponents and react.memo is used in functional components

35.React pure components -are the components that do not re-render when the value of props and state has been updated with the same values.

35.What is react suspense?
36.What is code spliting in react js?

37.Center a div in react vertically and horizantally?
<div style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height:'100vh' }} ><div>

EX2 : const App = () => {
  const styles = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
  };

  return (
    <div style={styles}>
      <h2>bobbyhadz.com</h2>
    </div>
  );
};

export default App;

38.Steps to create react project?
Install node js
Code editor(VS code);
Open terminal 
npx create-react-app my-proj


-----------------------------------------Redux /Flux/Context Apis------------------------------------------------

1.What is flux ?why we use it?
Flux is a architecture pattern to manage data flow through a react application,it provide centarlized store
In react we pass data from P-C comp .easy to manage the appliaction state and reduces complexity.
It has action,dispacther,store ,view(compoents)
to maintain dynamic data flow we can use flux

2.Difference between flux and redux?
Redux uses a 1 store(single-source of truth)(1 store for application)-best
Flux uses a multiple stores approach(multiple store for application)

3.1.Its a library or framework?
It is a architecture.folow the concept fo unidirectional data flow

4.what is redux?Why redux?
To manage the compoennets state we have redux ,it will sit on top of our appliaction and maintain data using a store 
Easy to keep track of our application over time-single source of truth
Using redux we can access the states in any compoennts 
We have actions and reducers

i.what are actions and reducers in redux?
actions-is object which contains a type of action and a payload(data field in the actions)

reducers -is only the way to update our states,
is a pure function it contains operartion that is performed on the state
It updates the state and responds with the new state.send back to view to update in DOM
A reducer must contain the action type.
dispatch -method will dispatch the action

5.Redux flow?

6.How to update state immutability?4 ways
i.Object.assign-from second object it will copy to first parameter object
const modifyShirt = (shirt, newColor, newSize) => {
  return Object.assign( {}, shirt, {
    color: newColor,
    size: newSize
  });
}

ii.spread operator
const modifyShirt = (shirt, newColor, newSize) => {
  return {
    ...shirt,
    color: newColor,
    size: newSize
  };
}

Immutability libraries
iii.Immutable.jsconfig
iv.immutability-helper


7.What is redux thunk ,saga,action creator?
*redux thunk is a middleware to perform Asynchrounous action(api calls) in our applications.
*it will act as a mediator between dispatching the action and handover those to reducers
by using applyMiddleware from redux 
We need to install redux-thunk
ex:api calls 


*Redux saga is also a middleware we can perform asynchronous actions in our application 
by using redux saga functions its easy to write the code and readable test
Compared to redux thunk its harder to test ,it need complex mocking of the fetch api,axios request and other functions

Saga                                                         thunk
 Saga uses Generators                                      Thunk allows Promises to deal with them,
 more powerfull but need to learn them                       As it uses promises simple to use
saga functions its easy to write the code and readable tes   its harder to test and complex mocking of the fetch api

7.2.can we use both saga and redux thunk?
yes 


8.How to create a store?
store is object which holds application state tree
useing createStore method and add it in a provider

if we use redux-toolkit then configStore method 

10.What is mapStateToProp and mapDispatch to prop and use of it?(cross question)
mapStateToProp-is used to get the data out of store.
mapDispatchToProps-mapDispatchToProps function used to update state in Redux store

10.1.Diff b/w mapStateToProp and useSelector ?
mapStateToProps passes down multiple values as props, 
useSelector takes the current state as an argument, returns the required data, and stores the returned value as a single variable instead of a prop.


11.What are middleware?Why we need it
It is a mediun between disapatcing the action and handling over action to the reducer.
With basic redux store we can perform synchronous updates only .Middlewares in Redux helps to handle asynchronous actions in your application.
Ex:For middleware redux thunk ,saga 
redux thunk-async action creators is recommendded to use as middleware


11.What is redux toolkit?Explain
redux Toolkit simplifies the Redux setup process,
we have api methods such as configureStore,createAction-, createReducer,createSlice(),createAsyncThunk

we have API methods such as
configureStore()-it wraps createStore by default,
it contains redux-thunk by default no need to add middleware and a
it automatically combined our slices
it enables redux-dev-tool

createAction-generates an action creator function for the given action type string.
createReducer-no need to add switch case builder callback we have
createSlice()-automatically genearte slice reducer
createAsyncThunk-accepts action type string and return promise and generate thunk dispatch based on action type promise
createEntityAdapter: generates a set of reusable reducers and selectors to manage normalized data in the store
 createSelector- utility from the Reselect library, re-exported for ease of use.
 createSelector function used to create memoized selectors.which is optimization

12.Why react is fast?
Because of virtual DOM
REact uses diffing algo internally

13.What is useSelector and useDispatch?
useSelector-Its a reducx hook which is used access the state from the store
useDispatch-Its a hook used to dispatch the actions ,used to access the redux stores dispatch function,
we need to pass the type of action to be dispatched


14.What are combineReducers?
combineReducers function allows you to combine multiple reducer functions into a single function that can be passed to the Redux store

import { combineReducers } from 'redux'
import todos from './todos'
import counter from './counter'

export default combineReducers({
  todos,
  counter
})

15.What is getState?
getState is used to get the store data using store.getState we can access all the states in store

16.What are action creators?
An action is simply an object that has two things: a type, and a payload. 
An action creator is simply a function, that just returns an action

export const addPost = ({ title, body }) => {
    return {
        title,
        body,
        id: shortid.generate()
    };
};

16.1.what is bindActionCreators()?
We use this to pass action creators down to components that is not aware of redux.
And we don't want to pass the dispatch or redux store to it
EX: bindActionCreators(actionCreators, dispatch)

17.What is the difference between useSelector and connect?
To be clear, connect is a high-order component, useSelector is a hook. 
Using useSelector can reduce the boilerplate code and embeds that logic within the components themselves




----------------------------------Routers and Protected Routers----------------------------
1.What is Concept of routing?
Recat provides "declarative routing" -means developers should provide path for conditionaly rendering the component
https://medium.com/nerd-for-tech/what-is-the-difference-between-react-router-and-conventional-routing-9b11159d92a4#:~:text=Unlike%20static%20routing%20where%20routes,or%20loading%20a%20new%20page.

2.What are protected routers?
protected routers only give access permission to authorized users.
Ex:if we have a login page once after authourization we are allowing the user to route next page
and hide the login page ,even if you type the url path in browser it wont allow


3.How is react router(Client-side routing) d/f from conventional routing?
react router-When ever we go to some screen only history stack (attribute ) is changes in react router,it uses client side routing
Conventinal routing -http request is sent to the server and it takes some time load the screen 

client-side-routing : components(spa) only required compoennt is loaded ,when URL is changed for routing,page will not referesh evvry time


4.What is switch?


---------------------------------Code spliting,suspens, layz loading----------------------------
https://blog.logrocket.com/optimizing-performance-react-app/#code-splitting-import

1.What is code spliting?Why we use it ?
Using code spliting-react allows us to split the larger bundle to multiple chunks using dynamic imports
It will increase the performance of application by reduces the load time
If we don't use this if application size increaces then bundlefile also increase , it will take time to load the application

*Dynamic imports we can do by React.lazy()-it load only required components

INSTEAD OF THIS
(import Home from "./components/Home";
import About from "./components/About";)

IMPORT LIKE THIS- React to load each component dynamically
const Home=React.Lazy(()=>import('./components/Home'))
const About=React.Lazy(()=>import('./components/About))

2.What is react suspense?
react suspens is used to display the loading text or indicator when react is waiting to render the components,
Fallback-it uses a fallback attribute ,to display the page is loading untill the page is rendered

Ex:
<React.Suspense fallback={<p>Loading page...</p>}>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
      </React.Suspense>

Ex:refer file

3.What is Conditional rendering?
There are multiple component in react to display those ,we use some conditions
ex:we login and logout button,both are separate components ,if user is logged in display logout btn ,if not display login button
for ex this situation can be handled by conditional rendering

We can use 
if/else
trenary operator
logical &&
swith case operator
conditioanl rendering with Enums

import React, { useState } from "react";

export default function App() {
  const [isLogin, setState] = useState(true);
  function Login() {
    return <button onClick={() => setState(false)}>login</button>;
  }
  function Logout() {
    return <button onClick={() => setState(true)}>logout</button>;
  }

  return (
    <div className="App">
      <h1>Company</h1>
      {isLogin ? <Login /> : <Logout />}
    </div>
  );
}


4.What are error boundries?

5.What are Portals?
React Portals are a powerful feature in React that allows you to render components outside the current React tree hierarchy. With portals, you can easily render elements into different parts of the DOM, such as modals, tooltips, or any other component that needs to break out of the component's container.

6.What is switch in check boxes?


------------------------------------------JS------------------------------------------------
1.Difference between var ,let and const?
var declarations are globally scoped or function scoped while let and const are block scoped.
 var variables can be updated and re-declared within its scope; 
 let variables can be updated but not re-declared; 
 const variables can neither be updated nor re-declared. They are all hoisted to the top of their scope

 2.What is hosting in js?
 Hoisting is JavaScript's default behavior of moving declarations to the top.
In js , a variable var can be declared after it has been used.

 31.Can let and const variables hoisted?
 Yes, Variables defined with let and const are hoisted to the top of the block, but not initialized.
 So This will result in a ReferenceError

32.Arrow functions cant be hoisted?
No,Arrow functions cant be hoisted because we cant call them before initialization

32.D/f b/w promises and aync await?
The promise involves chaining . then and . catch methods, which can make the code look complicated and hard to read. 
Async Await uses a simpler syntax that looks more like synchronous code.
Async/Await dose not have nay state,it returns the promise is reloved or rejected,Promises has 3 states
Error handling-Promise is done by .then and .catch methods,Async/awiat is done by try and catch block
chain-Promise harder to read,async/await easy to understand more synchronous way


33.Diffrence b/w asynchronous and synchronous functions?
Asynchrounous will not block the flow of code execution before its finisished (settimout function)
synchronous will block the flow of code execution before its got finished
State-Promise has states (pending,resolved and rejected), Async/Await dose not have nay state,it returns the promise is reloved or rejected
Error handling-Promise is done by .then and .catch methods,Async/awiat is done by try and catch block
chain-Promise harder to read,async/await easy to understand more synchronous way

34.Difference between setTimeout and setInterval functions?
The setTimeout() method is used to call a function after a certain period of time. 
The setInterval() Javascript method is used to call a function repeatedly at a specified interval of time. 
setTimeout() is cancelled by clearTimeout() method, and
setInterval() is cancelled by clearInterval() method.

35.What are closures in js?
Closure is a inner function having access to outer functions functions variables scope chain.

36.What are promises?
They are like real life promises.
Its an assurace that someting will be done.
It keeps tracks of asynchronous events has been executed or not .And determines what happen when event occures
It is an object which have 3 states mainly
1.Pending -initial state, before event occures
2.Fullfilled/Resolved-after operation completed successfully
3.Rejected-if operation as error during execution,promise fails

ex:
  const promise = new Promise(function (resolve, reject) {
    const string1 = "geeksforgeeks";
    const string2 = "geeksforgeeks";
    if (string1 === string2) {
      resolve();
    } else {
      reject();
    }
  });
 
  promise
    .then(function () {
      console.log("Promise resolved successfully");
    })
    .catch(function () {
      console.log("Promise is rejected");
    });

 37.What are asyn/awit?
Its an syntactic sugar for promises,a wrapper making the code execute more synchronously.

 ex: const helperPromise = function () {
    const promise = new Promise(function (resolve, reject) {
      const x = "geeksforgeeks";
      const y = "geeksforgeeks";
      if (x === y) {
        resolve("Strings are same");
      } else {
        reject("Strings are not same");
      }
    });
 
    return promise;
  };
 
  async function demoPromise() {
    try {
      let message = await helperPromise();
      console.log(message);
    } catch (error) {
      console.log("Error: " + error);
    }
  }
 
  demoPromise(); 

  38.What are callback functions?
  A callback function is a function passed as an argument into another/outer function
  It involved with outer function to complete the action 
 
 39.Can we use async await and promise together?
 Yes you can but it's better to avoid using both of them in one program because they have different purposes.

 40.What is DOM ?
 It represents web page as tree-like structure which allows JS to dynamically access AND MANIPULATE THE contents and structre of web page

 

41.Disable button if input field is empty ?
import React, { useState } from "react";

export default function App() {
  const [data, setData] = useState("");
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <input type="text" onChange={(e) => setData(e.target.value)} />
      <button disabled={data == ""}>Submit</button>
        </div>
  );
}

https://www.geeksforgeeks.org/controlled-vs-uncontrolled-components-in-reactjs/?ref=ml_lbp
https://dev.to/frontendengineer/10-reactjs-coding-exercises-with-codepen-exercise-and-solution--22k7