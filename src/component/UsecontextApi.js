import React,{useContext} from 'react'
import {GlobalInfo} from '../App'
import SuperChild from './SuperChild';
// React hook concept context,
// React contect API allows you to easily access data at different levels of
// Component tree,Without passing prop to every level
// Same as redux - passing components data from parent to child or super child or from any place to any place 
// Manily for data transfer between different components
// we can connect entrie project data or only some components data(why to use this)
// Transfer data from child to parent also we can do

const UsecontextApi = () => {
  const {AppColor}=useContext(GlobalInfo);
  console.warn(AppColor)
  return (
    <>
      <h1 style={{color:AppColor}}>child context</h1>
     <SuperChild/>
    </>
  )
}

export default UsecontextApi
