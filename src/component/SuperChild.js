import React,{useContext} from 'react';
import { GlobalInfo } from '../App';

const SuperChild = () => {
    const {AppColor,getDay}=useContext(GlobalInfo);
    const day="Sunday";

  return (
    <>
      <h2 style={{color:AppColor}}>Super Child</h2>
      <button onClick={()=>getDay(day)}>Click</button>
    </>
  )
}

export default SuperChild
