// 1.Alaway write it inside the component or function
// 2.Component name must be PascalCase (first letter should be upperCase)
// 3.we can directly import or we can directly write it using React.hookName
// 4.Don't call hooks inside loops,conditions,or nested functions

import React, { useState } from 'react'

// 2.component name (hookRule must be in pascal case)[mandatory if use hooks (but use the same always)]
const HookRule = () => {
  //4.We can't use directly entire hook like this instead we can use myName,setMyName and apply the conditions
  // if(true){ 
    const [myName,setMyName]=useState('Arshiya');
  // }
    
  return (
    <div>
      <h1>{myName}</h1>
    </div>
  )
}

export default HookRule;
