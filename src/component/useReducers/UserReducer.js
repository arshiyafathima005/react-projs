import React,{useReducer, useState} from 'react';

const initialState=0;
const reducer=(state,action)=>{
  console.log(state,action);
  if(action.type==="INCREMENT"){
    return state+1;
  }
  if(action.type==="DECREMENT"){
    return state-1;
  }
  // always reducer returns some thing
    return state;
}

export const UserReducer = () => {
    // const [count,setCount]=useState(0);
    // use reducer takses two parameter
    // dispatch will trigger the action method by using type 
  const [state,dispatch]=  useReducer(reducer,initialState);
  return (
    <>
     <div>
        <p>{state}</p>
        <div>
            <button onClick={()=>dispatch({type:"INCREMENT"}) }> + </button>
            <button onClick={()=>dispatch({type:"DECREMENT"})}> - </button>

        </div>
    </div>
    </>
   
  )
}
