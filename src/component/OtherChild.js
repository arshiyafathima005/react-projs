import React,{useContext} from 'react';
import { GlobalInfo } from '../App';


const OtherChild = () => {
    const {AppColor}=useContext(GlobalInfo);
   
  return (
    <div>
      <h1 style={{color:AppColor}}>Other child</h1>
    </div>
  )
}

export default OtherChild
