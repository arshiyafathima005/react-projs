import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

const Home = React.lazy(() => import("./components/Home"));
const About = React.lazy(() => import("./components/About"));

function App() {
  return (
    <Router>
      <React.Suspense fallback={<p>Loading page...</p>}>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="*" element={<h1>PAGE not found</h1>} />
      </React.Suspense>
    </Router>
  );
}

export default App;